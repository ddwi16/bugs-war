package com.example.bugswar.gameComponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import com.example.bugswar.R;
import com.example.bugswar.utility.MathUtil;

public class Bee {

    private float centerX, centerY;
    private float x, y;
    private float lastX, lastY;
    private float rotation;

    public boolean shouldDelete;

    private int shipHeight, shipWidth;
    private Paint shipPaint, shipFlamePaint;
    private Bitmap shipBitmap;
    private Matrix shipMatrix;
    private Direction driftDirection;
    private int driftRange;
    private int currentDrift;


    private enum Direction {
        OUTWARD,
        INWARD
    }

    public Bee() {
        shipPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shipPaint.setColor(0xFF7FD8FF);

        shipFlamePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shipFlamePaint.setColor(0xFFFF6060);
        shipFlamePaint.setColor(0xffE26660);

        shipMatrix = new Matrix();
        driftDirection = Direction.OUTWARD;
    }

    public void createShipBitmap(int screenWidth, Context context) {
        shipHeight = screenWidth/8;
        shipWidth = shipHeight * 3/4;

        int shipBottomCorner = shipHeight/6;

        Path shipPath = new Path();
        shipPath.moveTo(0, shipHeight-shipBottomCorner);
        shipPath.lineTo(shipWidth/2,0);
        shipPath.lineTo(shipWidth, shipHeight-shipBottomCorner);
        shipPath.lineTo(shipWidth-shipBottomCorner, shipHeight-shipBottomCorner/2);
        shipPath.lineTo(shipBottomCorner, shipHeight-shipBottomCorner/2);
        shipPath.close();

        Path flamePath = new Path();
        flamePath.moveTo(shipWidth/2, shipHeight + shipBottomCorner/2);
        flamePath.lineTo(shipWidth/2 - shipBottomCorner/2, shipHeight + shipBottomCorner/2);
        flamePath.lineTo(shipWidth/2 - shipBottomCorner/2, shipHeight - shipBottomCorner/2);
        flamePath.lineTo(shipWidth/2 + shipBottomCorner/2, shipHeight - shipBottomCorner/2);
        flamePath.lineTo(shipWidth/2 + shipBottomCorner/2, shipHeight + shipBottomCorner/2);
        flamePath.close();

        //shipBitmap = Bitmap.createBitmap(shipWidth, shipHeight, Bitmap.Config.ARGB_8888);
        //a = new SpaceGameApplication();
        shipBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.lebahsolo);
        Bitmap mutableBitmap = shipBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas shipCanvas = new Canvas(mutableBitmap);
        shipCanvas.drawPath(shipPath, shipPaint);
        shipCanvas.drawPath(flamePath, shipFlamePaint);
    }

    public void drawShip(Canvas canvas) {
        if(shipBitmap != null) {
            shipMatrix.reset();
            shipMatrix.setTranslate(x, y);
            shipMatrix.postRotate(rotation, x + shipHeight/2, y + shipWidth/2);
            canvas.drawBitmap(shipBitmap, shipMatrix, null);
        }
    }

    public void onFrame() {
        if (Math.abs(x - lastX) <= shipHeight / 16) {
            rotation = MathUtil.lerp(rotation, 0.f, 0.1f);
        }
    }

    public int getShipHeight() {
        return shipHeight;
    }

    public int getShipWidth() {
        return shipWidth;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        lastX = x;
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        lastY = y;
        this.y = y;
    }

    public void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public void setCenterY(float centerY) {
        this.centerY = centerY;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }
}
