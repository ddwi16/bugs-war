package com.example.bugswar.utility;

public class MathUtil {

    public static float lerp(float start, float end, float percent) {
        return start + percent * (end-start);
    }
}
