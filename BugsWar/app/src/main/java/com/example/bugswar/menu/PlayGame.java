package com.example.bugswar.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Looper;

import com.example.bugswar.R;
import com.example.bugswar.controller.GameController;
import com.example.bugswar.utility.SharedPrefConstants;
import com.example.bugswar.view.SpaceAnimationView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

public class PlayGame extends AppCompatActivity {

    private static Context context;
    private GameController controller;
    private SpaceAnimationView spaceView;

    private AdView mAdView;

    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PlayGame.context = getApplicationContext();
        setContentView(R.layout.activity_main);
        //setContentView(game);

        spaceView = (SpaceAnimationView) findViewById(R.id.view_spaceAnimationView);

        controller = new GameController();
        controller.resetGame();

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7643931051857186/4348051730");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                mInterstitialAd.loadAd(new AdRequest.Builder().build());

            }
        });
        //AdView adView = new AdView(this);
        //adView.setAdSize(AdSize.BANNER);
        //adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }


    @Override
    protected void onResume() {
        SharedPreferences prefs = getSharedPreferences(SharedPrefConstants.PREF_NAME, MODE_PRIVATE);

        int highScore = prefs.getInt(SharedPrefConstants.KEY_HIGH_SCORE, -1);
        if(highScore >= 0) {
            controller.setHighScore(highScore);
            showInterstitial();
        }
        if(spaceView != null) {
            spaceView.onActivityResume();
        }

        super.onResume();
    }

    public void showInterstitial(){
        if (Looper.myLooper() != Looper.getMainLooper()){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    doshowInterstitial();
                }
            });
        }
        else {
            doshowInterstitial();
        }
    }
    private void doshowInterstitial() {
        // Show the ad if it's ready. Otherwise toast and restart the game.
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

    @Override
    protected void onPause(){
        SharedPreferences pref = getSharedPreferences(SharedPrefConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(SharedPrefConstants.KEY_HIGH_SCORE, controller.getHighScore());
        editor.apply();

        if(spaceView != null) {
            spaceView.onActivityPause();
        }

        super.onPause();
    }
}
