package com.example.bugswar.gameComponent;

public class Explosion {
    public float diameter;
    public float x, y;
    public int color;
    public int alpha;
    public int radius;
    public boolean shouldDelete;

    public int radiusMult;
    public int duration;
}
