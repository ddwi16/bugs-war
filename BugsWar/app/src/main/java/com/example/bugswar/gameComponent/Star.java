package com.example.bugswar.gameComponent;

public class Star {

    public float x, y;
    public int color;
    public int radius;
    public float speed;
}
