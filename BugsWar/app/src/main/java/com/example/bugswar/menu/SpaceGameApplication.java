package com.example.bugswar.menu;

import android.app.Application;
import com.example.bugswar.controller.Controller;
import com.example.bugswar.controller.GameController;

import java.util.HashMap;

public class SpaceGameApplication extends Application {
    private static HashMap<Integer, Controller> controllerHashMap = new HashMap<>();


    @Override
    public void onCreate() {
        super.onCreate();
        GameController controller = new GameController();
        controllerHashMap.put(Controller.GAME_CONTROLLER, controller);
    }


    public static Controller getController(int controller) {
        return controllerHashMap.get(controller);
    }
}
