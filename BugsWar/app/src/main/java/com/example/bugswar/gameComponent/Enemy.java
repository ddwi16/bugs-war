package com.example.bugswar.gameComponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;

import com.example.bugswar.R;

public class Enemy {

    public float x, y;
    public float speed;
    public float rotation;
    public boolean shouldDelete = false;
    public float diameter;

    private Paint enemyPaint, enemyBorderPaint;
    private Bitmap enemyBitmap;
    private Matrix enemyMatrix = new Matrix();

    public Enemy(float diameter, Context context) {
        this.diameter = diameter;

        enemyPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        enemyPaint.setStyle(Paint.Style.STROKE);
        enemyPaint.setStrokeWidth(diameter / 8);
        enemyPaint.setColor(0xff323299);

        enemyBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        enemyBorderPaint.setColor(0xE6FFFFFF);

        if(enemyBitmap == null) {
            createEnemyBitmap(context);
        }
    }

    public void createEnemyBitmap(Context context) {
        Path enemyPath = new Path();
        enemyPath.moveTo(0,0);
        enemyPath.lineTo(diameter/3, diameter);
        enemyPath.lineTo(diameter/2, diameter/2);
        enemyPath.moveTo(diameter/3, 0);
        enemyPath.lineTo(diameter * 2/3, diameter);
        enemyPath.lineTo(diameter,0);

        //enemyBitmap = Bitmap.createBitmap((int) diameter, (int) diameter, Bitmap.Config.ARGB_8888);
        enemyBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.redbugsolo);
        Bitmap mutableBitmap = enemyBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
        canvas.drawRect(0,0, diameter, diameter, enemyBorderPaint);
    }

    public void drawEnemy(Canvas canvas) {
        enemyMatrix.reset();
        enemyMatrix.setTranslate(x,y);
        enemyMatrix.postRotate(rotation, x + diameter/2, y + diameter/2);
        canvas.drawBitmap(enemyBitmap, enemyMatrix,null);
    }

    public float getDiameter() {
        return diameter;
    }

    public void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY (float y) {
        this.y = y;
    }
}
