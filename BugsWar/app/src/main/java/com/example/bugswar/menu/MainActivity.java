package com.example.bugswar.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.bugswar.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        Button startButton = findViewById(R.id.start_Button);
        startButton.setOnClickListener(this);

        Button creditButton = findViewById(R.id.credit_Button);
        creditButton.setOnClickListener(this);

        Button exitButton = findViewById(R.id.exit_Button);
        exitButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_Button:
                Intent intent = new Intent(this, PlayGame.class);
                startActivity(intent);
                break;
            case R.id.credit_Button:
                Intent intent1 = new Intent(this, CreditMenu.class);
                startActivity(intent1);
                break;
            case R.id.exit_Button:
                finish();
                System.exit(0);
                break;
        }
    }
}
