package com.example.bugswar.utility;

public class SharedPrefConstants {

    public static final String PREF_NAME = "pref_name";
    public static final String KEY_HIGH_SCORE = "high_score";
}
