package com.example.bugswar.gameComponent;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;

import com.example.bugswar.R;

public class Treasure {

    public float x, y;
    public float speed;
    public float diameter;
    public boolean shouldDelete = false;

    private Paint treasurePaint;
    private Bitmap treasureBitmap;
    private Matrix treasureMatrix = new Matrix();

    public Treasure(float diameter, Context context) {
        this.diameter = diameter;

        treasurePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        treasurePaint.setColor(0xFF72FF66);

        if(treasureBitmap == null) {
            createTreasureBitmap(context);
        }
    }

    public void createTreasureBitmap(Context context) {
        //treasureBitmap = Bitmap.createBitmap((int) diameter, (int) diameter, Bitmap.Config.ARGB_8888);
        treasureBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.honeybee);
        Bitmap mutableBitmap = treasureBitmap.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(mutableBitmap);
        canvas.drawCircle(diameter/2, diameter/2, diameter/2, treasurePaint);
    }

    public void drawTreasure(Canvas canvas) {
        treasureMatrix.reset();
        treasureMatrix.setTranslate(x, y);

        canvas.drawBitmap(treasureBitmap, treasureMatrix, null);
    }
}
